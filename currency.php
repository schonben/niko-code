<?php

require("global/init.inc");

$xml_parser = xml_parser_create();

if (!($fp = fopen($setting[currency_xml], "r"))) {
    die("could not open XML input");
}

while (!feof($fp)) {
	$data .= fread($fp, 1024);
}
fclose($fp);
xml_parse_into_struct($xml_parser, $data, $vals, $index);
xml_parser_free($xml_parser);

foreach ($index["CUBE"] as $i) {
	if (isset($vals[$i][attributes][TIME])) {
		$tim = $vals[$i][attributes][TIME];
		echo "time: ".$vals[$i][attributes][TIME]."\n";
	}
	if ($vals[$i][level]==4) {
		$id = $vals[$i][attributes][CURRENCY];
		$query = "
			select	id
			from		$setting[schema].currency
			where		id = '$id'
			";
		$ch = pg_query($conn,$query);
		if (pg_num_rows($ch) > 1) {
			$query = "
				delete
				from		$setting[schema].currency
				where		id = '$id'
				";
			pg_query($conn,$query);
		}
		if (pg_num_rows($ch) == 0) {
			$query = "
				insert into $setting[schema].currency (
					id
				) values (
					'$id'
				)";
			pg_query($conn,$query);
		}
		$query = "
			update	$setting[schema].currency
			set		rate = ".$vals[$i][attributes][RATE].",
						upd = '$tim'
			where		id = '$id'
			";
		pg_query($conn,$query);
	}
}

?>