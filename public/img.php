<?php

/*
Dynamic cacheing image resizer / thumbnail generator
Arguments:
src (required) - Relative path to filename
size - Maximum heigth and witdh of new image
h - Maximum height of image, or absolute height if crop=1, overrides size, defaults to height of original image 
w - Maximum width of image, or absolute width if crop=1, overrides size, defaults to width of original image
crop - Should image be cropped to fit into h/w, default 0
format - Specifies output format, options are jpeg/png/gif. Defaults to same as original,
or png if not one of the three options.
qual - Compression quality, defaults to 95. Used on format jpeg only.
Note:
Can't enlarge images. Height or width given that exceeds original images dimensions
are truncated to that of the original image.

Written by Niklas Schönberg <niko@uplink.fi>

Released under:
Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported
http://creativecommons.org/licenses/by-nc-sa/3.0/
*/

// Needs $setting[imgcache] for path to image cache
$setting['imgcache']="/var/www/imgcache";
$setting['imgcache']="/var/www/imgcache";

$e = explode(".",$_SERVER["SERVER_NAME"]);

if (!trim($_GET['src']))
	exit;
$img = realpath("../images/".$_GET['src']);
if (!file_exists($img)) {
	exit;
}


if (isset($_GET['h']))
	$h = $_GET['h'];
else
	$h = $_GET['size'];
if (isset($_GET['w']))
	$w = $_GET['w'];
else
	$w = $_GET['size'];

$size = getimagesize($img);
$width = $size[0];
$height = $size[1];

if ($w < 1 || $w > $width)
	$w = $width;
if ($h < 1 || $h > $height)
	$h = $height;

$path_parts = pathinfo($img);
$file = $path_parts['basename'];
$woext = $path_parts['filename'];
$ext = $path_parts['extension'];
if (!is_numeric($_GET['qual']))
	$_GET['qual'] = 95;


if ($size['mime'] != "image/png" && $size['mime'] != "image/gif" && $size['mime'] != "image/jpeg") {
	$size['mime'] = "image/png";
	$ext = "png";
}

switch ($_GET['format']) {
	case "jpeg":
	case "jpg":
		$size['mime'] = "image/jpeg";
		$ext = "jpg";
	break;
	case "png":
		$size['mime'] = "image/png";
		$ext = "png";
	break;
	case "gif":
		$size['mime'] = "image/gif";
		$ext = "gif";
	break;
}

$cachefile = str_replace(".","_",$file)."_".filesize($img)."_".$_GET['qual']."_".$h."_".$w."_".$_GET['crop'];
$cache = $setting['imgcache']."/".$cachefile.".".$ext;

if (!file_exists($cache)) {
	// ImageMagick
	$handle = new Imagick();			
	$handle->readImage($img);
	if ($_GET[crop]) {
		$ratio_h = $height / $h;
		$ratio_w = $width / $w;
		if ($ratio_h > $ratio_w) {
			$ratio_diff = $ratio_h / $ratio_w;
			$ss = round($height / $ratio_diff);
			$diff = ($height - $ss) / 2;
			$handle->cropImage($width,$ss,0,$diff);
		} else {
			$ratio_diff = $ratio_w / $ratio_h;
			$ss = round($width / $ratio_diff);
			$diff = ($width - $ss) / 2;
			$handle->cropImage($ss,$height,$diff,0);
		}
	} else {
		$ratio = $height / $width;
		$mh = $h;
		$mw = $w;
		$w = round($mh / $ratio);
		if ($w > $mw) {
			$w = $mw;			
			$h = round($mw * ratio);
		}
	}
	$handle->thumbnailImage($w,$h);
	if (isset($flip))
		$handle->blurImage(5,3);
	$handle->setCompression(Imagick::COMPRESSION_JPEG);
	$handle->setCompressionQuality($_GET['qual']);
	$handle->setImageFileName($cache);
	$handle->writeImage();
	$handle->destroy();
}

header("Content-Type: $size[mime]");
header("Cache-control: public");
header("Pragma: public");
header("Expires: ".date("D, d M Y H:i:s T",time()+86400));
header("Content-Disposition: inline; filename=$woext.$ext");
header("Content-Length: ".filesize($cache));
header("Last-Modified: ".date("D, d M Y H:i:s T",filemtime($cache)));
readfile($cache);

?>
