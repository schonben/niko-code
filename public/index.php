<?php

require("../global/init.inc");
require("../global/init_display.inc");

include("../headers/header.inc");
$keys = array_keys($menu);
if (isset($menu[$_REQUEST['section']]))
    $section = $_REQUEST['section'];
else 
    $section = $keys[0]; // Default to first item in menu

$submenu = $menu[$section];
$smarty->assign("section",$section);
$smarty->assign("submenu",$submenu);
$keys = array_keys($submenu);

if (isset($submenu[$_REQUEST['template']]))
    $template = $_REQUEST['template'];
else
    $template = $keys[0]; // Default to first subitem

if (file_exists("../pages/{$template}.page")) {
    $raw = file_get_contents($setting['apath']."/pages/{$template}.page");
    $sOutput = bbcode(nl2br($raw,true));
} else if (file_exists("../templates/{$template}.inc")) {
    // Save all output from called php template
    ob_start();
    include("../templates/{$template}.inc");
    $sOutput = ob_get_contents();
    ob_clean();
}
// Assign output to smarty
$smarty->assign("scriptoutput",$sOutput);

// Check that called template is valid, index.tpl will later include it.
if ($smarty->TemplateExists("{$template}.tpl") && $template != "index") {
    $smarty->assign("template",$template);
} else {
    $smarty->assign("template","");
}

$smarty->display("index.tpl");

?>