<?php

require("../global/init.inc");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if ($template != "main" && md5($_COOKIE["check_cookie"]) == $cval) {
		require("$template.inc");
		header("Location: http://".$_SERVER["HTTP_HOST"].$setting[rpath]."/$returl");
	} else {
		echo "Checksum error";
	}
} else {
	echo "Method error, use POST";
}

?>