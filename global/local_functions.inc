<?php

function linkformat ($text) {
    return preg_replace("/[^a-z0-9]/", "", strtolower($text));
}


function bbcode ($text) {
    while (($s = stripos($text,"[code]")) && ($e = stripos($text,"[/code]",$s))) {
        $sect = substr($text,$s,$e-$s+7);
        $newtext = str_replace("<br />","",$sect);
        $newtext = str_replace("<br/>","",$sect);
        $newtext = trim(eregi_replace("\[/?code\]","",$newtext)," \n\r\t");
        $newtext = html_entity_decode($newtext);
        $newtext = safe_xml_entities($newtext);
        //$newtext = str_replace("&#9;","&nbsp;&nbsp;&nbsp;",$newtext);
        $newtext = "<code>".nl2br($newtext)."</code>";
        $text = str_replace($sect,$newtext,$text);
    }

    $text = preg_replace('%\[(/?)([buis])\]%','<\1\2>',$text);
    $text = preg_replace('%\[img( [^"\]]+)?\]([^\n[]+)\[/img\]%i','<img class="bbimg\1" alt="" src="\2" />',$text);
    // internal links
    $text = preg_replace("%\[url\](/|\?)([^[]+)\[/url\]%i","<a href='http\\1\\2' class='bblink'>\\1\\2</a>",$text);
    $text = preg_replace("%\[url=&#34;(/|\?)([^]]+)&#34;\]([^[]+)\[/url\]%i","<a href='\\1\\2' class='bblink'>\\3</a>",$text);
    $text = preg_replace("%\[url=(/|\?)([^]]+)\]([^[]+)\[/url\]%i","<a href='\\1\\2' class='bblink'>\\3</a>",$text);
    // external links
    $text = preg_replace("%\[url\](http(s)?://)?([^[]+)\[/url\]%i","<a href='http\\2://\\3' class='bblink'>\\3</a>",$text);
    $text = preg_replace("%\[url=&#34;(http(s)?://)?([^]]+)&#34;\]([^[]+)\[/url\]%i","<a href='http\\2://\\3' class='bblink'>\\4</a>",$text);
    $text = preg_replace("%\[url=(http(s)?://)?([^]]+)\]([^[]+)\[/url\]%i","<a href='http\\2://\\3' class='bblink'>\\4</a>",$text);
    $text = preg_replace("%\[list\](<br ?/>)?%i","<ul type='disc'>",$text);
    $text = preg_replace("%\[list=(.)\](<br ?/>)?%i","<ul type='\\1'>",$text);
    $text = preg_replace("%\[/list\](<br ?/>)?%i","</ul>",$text);
    $text = preg_replace("%\[olist\](<br ?/>)?%i","<ol>",$text);
    $text = preg_replace("%\[olist=(.)\](<br ?/>)?%i","<ol type='\\1'>",$text);
    $text = preg_replace("%\[/olist\](<br ?/>)?%i","</ol>",$text);
    $text = preg_replace("%\[\*\]([^\n]+)(<br ?/>|\n)%i","<li>\\1</li>",$text);
    $text = preg_replace("%\[color=([^];]+)\]%i","<inline style='color: \\1;'>",$text);
    $text = preg_replace("%\[size=([^];]+)\]%i","<inline style='font-size: \\1;'>",$text);
    $text = preg_replace("%\[/(color|size)\]%i","</inline>",$text);
    $text = preg_replace("%(<br ?/>)*\[table\](<br ?/>)*%i","<table class='bb'><tr><td>",$text);
    $text = preg_replace("%(<br ?/>)*\[row\](<br ?/>)*%i","</td></tr><tr><td>",$text);
    $text = preg_replace("%(<br ?/>)*\[cell\](<br ?/>)*%i","</td><td>",$text);
    $text = preg_replace("%(<br ?/>)*\[/table\](<br ?/>)*%i","</td></tr></table>",$text);
    $r = rand(10000,99999);
    $text = preg_replace("%\[spoiler\]([^[]+)\[/spoiler\]%i","<span class='spoiler'><span class='spoiler_button' onclick='document.getElementById(\"spoiler_$r\").style.visibility=\"visible\";'>Spoiler! </span><span id='spoiler_$r' class='spoiler_text' style='visibility: hidden;'>\\1</span></span>",$text);

    return checktags($text);
}

function checktags ($text,$tags="b|u|i|s|div|a") {
    $stack = Array();
    $temp = Array();
    $offset = 0;
    $taga = explode("|",$tags);
    // Loop through all tags in text
    while ($offset !== false) {
        preg_match("/<(\/?)(".$tags.")( [^>]+)*>/",$text,$matches,PREG_OFFSET_CAPTURE,$offset);
        if (count($matches)) {
            // If there is still matches
            $offset = $matches[0][1];
            if ($matches[1][0] != "/") {
                // If tag is opening tag add it to the stack
                $stack[] = $matches[2][0];
                $offset += strlen($matches[0][0]);
            } else {
                // If tag is closing tag, see if it matches last tag in stack and remove the tag from the stack             
                $last = array_pop($stack);
                if ($matches[2][0] !== $last) {
                    if (array_search($matches[2][0],$stack) !== false) {
                        // If tag exists in the stack, add all needed closing tags
                        do {
                            array_unshift($temp,$last);
                            $tag = "</$last>";
                            $text = substr_replace($text,$tag,$offset,0);
                            $offset += strlen($tag);                            
                            $last = array_pop($stack);
                        } while ($matches[2][0] !== $last);
                        // Return closed tags to stack and to the open them in the text
                        $offset += strlen($matches[0][0]);
                        foreach ($temp as $tg) {
                            $stack[] = $tg;
                            $tag = "<$tg>";
                            $text = substr_replace($text,$tag,$offset,0);
                            $offset += strlen($tag);
                        }
                        $temp = Array();
                    } else {
                        // If tag doesn't exsist in stack, disable tag and mark i invalid
                        $tag = "{/".$matches[2][0]."}";
                        $text = substr_replace($text,$tag,$offset,strlen($matches[0][0]));
                        // If tag exsists, return last tag to stack
                        if (strlen($last))                      
                            $stack[] = $last;
                    }
                } else {
                    $offset += strlen($matches[0][0]);
                }
            }
        } else {
            // If no matches, end loop by setting $offset to false
            $offset = false;
        }
    }
    // Loop remainging stack and close all open tags in order
    $close = "";
    foreach ($stack as $stag) {
        $close = "</$stag>".$close;
    }
    $text .= $close;
    // Finally turn all remaining tags red to show they are mismatched  
    $text = preg_replace("/{(\/?)(".$tags.")}/","<span style='color: red;'>[\\1\\2]</span>",$text);

    return $text;
}

?>