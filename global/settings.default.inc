<?php

// This file should be renamed (or copied if you plan to commit to github) to settings.inc 

$dbh=""; // Database hostname
$dbu=""; // Database username
$ds=""; // Database name

$setting['schema']=""; // Database schema
$setting['site_uri']=""; // The URL of the site
$setting['title']=""; // Site title
$setting['subtitle']=""; // Site subtitle
$setting['rpath']=""; // Relative path
$setting['apath']=""; // Absolute path
$setting['tmppath']=""; // A temporary filedump path
$setting['imgpath']=""; // Relative path to images
$setting['dev']=1; // Is this a dev version?
$setting['analytics']=""; // Google Analytics ID
$setting['currency_xml']="http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"; // URL to exchange rate XML file
$setting['css'] = Array("style"); // List of style sheets to use

$locale="en"; // Default locale

?>
