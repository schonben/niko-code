<?php

$tps = array("currency","temperature","length","energy","power","mass","pressure","area","volume");

if (!isset($tp))
	$tp = "currency";

echo "
	<script src='/javascript/convert.js' type='text/javascript'></script>
	<form action='' method='get'>
		<input type='hidden' name='template' value='$template'/>
		<div>
			<select name='tp' onchange='this.form.submit();'>
			";
		foreach ($tps as $tpi) {
			if ($tp == $tpi) $sel = " selected='selected'"; else $sel = "";
			echo "<option value='$tpi'$sel>".UCfirst($tpi)."</option>";
		}
		echo "
			</select>
		</div>
	</form>
	";
switch ($tp) {
	case "currency":
		$jscall = "updCurr(this.form);";
		$extra = "<h3>1 <span id='fromunit'>EUR</span> = <span id='torate'>1</span> <span id='tounit'>EUR</span>.</h3>";
		$query = "
			select	id,
						longname as name,
						rate,
						upd,
						euro
			from		$setting[schema].currency
			order by	euro, id
			";
		$cu = pg_query($conn,$query);
		$opt = "<option value='1'>EUR - Euro</option>";
		while ($cu_r = pg_fetch_array($cu)) {
			$opt .= "<option value='$cu_r[rate]'>$cu_r[id] - $cu_r[name]</option>";
		}
	break;
	case "temperature":
		$jscall = "updGen(this.form,0);";
		$extra = "";
		$unit[] = Array("Kelvin",0,1);
		$unit[] = Array("Celsius",-273.15,1);
		$unit[] = Array("Fahrenheit",-459.67,1.8);
		$unit[] = Array("Rankine",0,1.8);
		$unit[] = Array("Delisle",+559.725,-1.5);
		$unit[] = Array("Newton",-90.14,0.33);
		$unit[] = Array("Réaumur",-218.52,0.8);
		$unit[] = Array("Rømer",-135.90,0.525);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
	case "length":
		$jscall = "updGen(this.form,1);";
		$extra = "";
		$unit[] = Array("Metre",0,1);
		$unit[] = Array("Centimetre",0,0.01);
		$unit[] = Array("Millimetre",0,0.001);
		$unit[] = Array("Kilometre",0,1000);
		$unit[] = Array("Micrometre",0,0.000001);
		$unit[] = Array("Inch",0,0.0254);
		$unit[] = Array("Foot",0,0.3048);
		$unit[] = Array("Yard",0,0.9144);
		$unit[] = Array("Mile",0,1609.344);
		$unit[] = Array("Fathom",0,1.8288);
		$unit[] = Array("Nautical mile",0,1852);
		$unit[] = Array("Furlong",0,201.1680);
		$unit[] = Array("Astronomical Unit",0,149600000000);
		$unit[] = Array("Lightyear",0,9461000000000000);
		$unit[] = Array("Parsec",0,30857000000000000);
		$unit[] = Array("Royal Cubit",0,0.524);
		$unit[] = Array("Roman cubitus",0,0.4445);
		$unit[] = Array("League",0,5556);
		$unit[] = Array("Chain",0,20.1168);
		$unit[] = Array("Rod",0,5.0292);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
	case "energy":
		$jscall = "updGen(this.form,1);";
		$extra = "";
		$unit[] = Array("Joule",0,1);
		$unit[] = Array("Watt-second",0,1);
		$unit[] = Array("Newtonmetre",0,1);
		$unit[] = Array("Kilowatt-hour",0,3600000);
		$unit[] = Array("Calorie",0,4.1868);
		$unit[] = Array("Kilocalorie",0,4186.8);
		$unit[] = Array("Electronvolt",0,0.0000000000000000001602);
		$unit[] = Array("Foot-pounds",0,1.3558179483314004);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
	case "power":
		$jscall = "updGen(this.form,1);";
		$extra = "";
		$unit[] = Array("Watt",0,1);
		$unit[] = Array("Kilowatt",0,1000);
		$unit[] = Array("Metric horsepower",0,735.49875);
		$unit[] = Array("Mechanical horsepower",0,745.6999);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
	case "mass":
		$jscall = "updGen(this.form,1);";
		$extra = "";
		$unit[] = Array("Kilogram",0,1);
		$unit[] = Array("Gram",0,0.001);
		$unit[] = Array("Ounce (avoirdupois)",0,0.028349523125);
		$unit[] = Array("Ounce (troy)",0,0.0311034768);
		$unit[] = Array("Grain",0,0.06479891);
		$unit[] = Array("Pound",0,0.45359237);
		$unit[] = Array("Stone",0,6.35);
		$unit[] = Array("Long ton (British)",0,1016.0469088);
		$unit[] = Array("Short ton (American)",0,907.18474);
		$unit[] = Array("Tonne (Metric)",0,1000);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
	case "pressure":
		$jscall = "updGen(this.form,1);";
		$extra = "";
		$unit[] = Array("Pascal",0,1);
		$unit[] = Array("Bar",0,100000);
		$unit[] = Array("Technical atmosphere",0,98066.5);
		$unit[] = Array("Atmosphere",0,101325);
		$unit[] = Array("Torr",0,133.322);
		$unit[] = Array("Pound-force per square inch",0,6894.76);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
	case "area":
		$jscall = "updGen(this.form,1);";
		$extra = "";
		$unit[] = Array("Square metre",0,1);
		$unit[] = Array("Are",0,100);
		$unit[] = Array("Hectare",0,10000);
		$unit[] = Array("Square kilometre",0,1000000);
		$unit[] = Array("Square Inch",0,0.00064516);
		$unit[] = Array("Square foot",0,0.09290304);
		$unit[] = Array("Square yard",0,0.83612736);
		$unit[] = Array("Acre",0,4046.8564224);
		$unit[] = Array("Square mile",0,2589988.1103);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
	case "volume":
		$jscall = "updGen(this.form,1);";
		$extra = "";
		$unit[] = Array("Litre",0,1);
		$unit[] = Array("Centilitre",0,0.01);
		$unit[] = Array("U.S. liquid gallon",0,3.785411784);
		$unit[] = Array("U.S. dry gallon",0,4.40488377086);
		$unit[] = Array("Imperial (UK) gallon",0,4.54609);
		$unit[] = Array("Imperial pint",0,0.56826125);
		$unit[] = Array("United States liquid pint",0,0.473176473);
		$unit[] = Array("United States dry pint",0,0.5506104713575);
		$unit[] = Array("Metric pint",0,0.550);
		$unit[] = Array("Imperial fluid ounce",0,0.0284130625);
		$unit[] = Array("U.S. customary fluid ounce",0,0.0295735295625);
		$unit[] = Array("U.S. food labeling fluid ounce",0,0.03);
		foreach ($unit as $u) {
			$opt .= "<option value='$u[1]|$u[2]'>$u[0]</option>";
		}
	break;
}
echo "
	<form action='' method='get'>
		<div class='box'>
			<h1>".UCFirst($tp)." Converter</h1>
			<div>
				<label for='fr'>From</label>
				<select id='fr' name='fr' onchange='$jscall'>
					$opt
				</select>
				<input type='text' size='10' name='f' value='1' onkeyup='$jscall' onchange='$jscall'/>
			</div>
			<div>		
				<label for='too'>To</label>
				<select id='too' name='to' onchange='$jscall'>
					$opt
				</select>		
				<input type='text' size='10' name='t' value='1' readonly='readonly'/>
			</div>
			$extra		
		</div>
	</form>
	";




?>