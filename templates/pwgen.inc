<?php

if ($_GET[len] < 1)
	$_GET[len] = 8;
if ($_GET[len] > 80)
	$_GET[len] = 80;

if ($_GET[num] || !isset($_GET[gen]))
	$nsel = " checked='checked'";
if ($_GET[alp] || !isset($_GET[gen]))
	$asel = " checked='checked'";
if ($_GET[mix])
	$msel = " checked='checked'";


echo "
	<form action='' method='get'>
		<input type='hidden' name='template' value='$_GET[template]'/>
		<div>
			<div>
				<label for='len'>Length</label>
				<input type='text' id='len' name='len' value='$_GET[len]' size='2'/>
			</div>
			<div>
				<label for='num'>Numbers</label>
				<input type='checkbox' id='num' name='num' value='1'$nsel/>
			</div>
			<div>
				<label for='alp'>Letters</label>
				<input type='checkbox' id='alp' name='alp' value='1'$asel/>
			</div>
			<div>
				<label for='mix'>Mixed Case</label>
				<input type='checkbox' id='mix' name='mix' value='1'$msel/>
			</div>
			<div>
				<input type='submit' name='gen' value='Generate'/>
			</div>
		</div>
	</form>
	";

$nr = 5;
if (($_GET[num] || $_GET[alp]) && isset($_GET[gen])) {
	$chars = "";
	if ($_GET[num])
		$chars .= "0123456789";
	if ($_GET[alp])
		$chars .= "abcdefghijklmnopqrstuvwxyz";

	$c = preg_split('//', $chars, -1, PREG_SPLIT_NO_EMPTY);

	// Loop to generate $nr passwords
	for ($pwn=0;$pwn<$nr;$pwn++) {
		$pw = "";
			
		// Loop to generate digit
		for ($i=0;$i<$_GET[len];$i++) {
			$r = rand(0,count($c)-1);
			if (rand(0,1) && $_GET[mix])
				$pw .= strtoupper($c[$r]);
			else
				$pw .= $c[$r];

		}
		echo "
			<form action='' method='get'>
				<div>
					<label for='pw_$pwn'>".($pwn+1)."</label>
					<input id='pw_$pwn' type='text' name='output' value='$pw' size='$len'/>
				</div>
			</form>		
			";
	}
}


?>
