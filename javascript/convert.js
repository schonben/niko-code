
function updCurr (f) {
	f.t.value = round((f.f.value / f.fr.value) * f.to.value,3);
	changeRate(f);
 	return true;
}
function changeRate (f) {
	fu = document.getElementById("fromunit");
	tu = document.getElementById("tounit");
	tr = document.getElementById("torate");
	fu.removeChild(fu.childNodes[0])
	fu.appendChild(document.createTextNode(f.fr.options[f.fr.selectedIndex].text.substr(0,3)));
	tu.removeChild(tu.childNodes[0])
	tu.appendChild(document.createTextNode(f.to.options[f.to.selectedIndex].text.substr(0,3)));
	tr.removeChild(tr.childNodes[0])
	tr.appendChild(document.createTextNode(round(((1 / f.fr.value) * f.to.value),5)));
}

function updGen(f,pn) {
	fr = f.fr.value.split("|");
	to = f.to.value.split("|");
	if (pn == 0) {
		divf = parseFloat(fr[1]);
		divt = parseFloat(to[1]);
	} else {
		divf = 1 / parseFloat(fr[1]);
		divt = 1 / parseFloat(to[1]);
	}
	mid = (parseFloat(f.f.value) - parseFloat(fr[0])) / divf;
	target = (mid * divt) + parseFloat(to[0]);
	f.t.value = round(target,2);

}

function round (n,d) {
	nn = Math.pow(10,d);
	return Math.round( n * nn ) / nn;
}